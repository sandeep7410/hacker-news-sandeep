from app.models import User,Post,Comment
from app import db
from app import celery
from app import create_app
import json
import os

@celery.task(bind=True)
def download_content(self,username):
    app=create_app()
    db.create_all(app=create_app())
    with app.app_context():
        user=User.query.filter_by(username=username).first()
        posts=user.post.all()
        dic=[]
        total=len(posts)
        message=''
        i=0
        for post in posts:
            i=i+1
            message='{}%'.format(i*100/total)
            self.update_state(state='PROGRESS', meta={'result': message})
            dic.append(post._asdict())
        filename = "data/{}.json".format(username)
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename,'w+') as file:
            json.dump(dic,file)
        return {'state':'SUCCESS','result': '100%'}

