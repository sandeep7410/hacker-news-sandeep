from app import db
from app.background import bp
from flask import render_template, request, url_for, redirect, flash, abort
from flask_login import login_required, current_user
from app.models import Post, Comment, User
from app.background.tasks import download_content
from flask import send_file, make_response, send_from_directory
from flask import jsonify
from app import celery


@bp.route('/download', methods=['POST'])
@login_required
def download():
    if request.method == 'POST':
        task = download_content.apply_async((current_user.username,))
        return jsonify({}), 202, {'Location': url_for('background.taskstatus', task_id=task.id)}
    else:
        return redirect(url_for('main.index'))


@bp.route('/download/<username>.json')
@login_required
def download_json(username):
    if username == current_user.username:
        path = '../data/{}.json'.format(current_user.username)
        try:
            return send_file(path, as_attachment=True)
        except:
            return abort(404)

    else:
        flash('Can\'t download the file...')
        abort(403)


@bp.route('/status/<task_id>')
@login_required
def taskstatus(task_id):
    task = download_content.AsyncResult(task_id)
    response={
    'state':task.state
    }
    if task.info:
        response={
            'state':task.state,
            'result':task.info.get('result',0)
        }
    return jsonify(response)

