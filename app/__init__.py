from flask import Flask,request,current_app
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from config import Config
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from elasticsearch import Elasticsearch
from celery import Celery
import os
from logging.handlers import RotatingFileHandler
import logging


db=SQLAlchemy()
migrate=Migrate()
bootstrap=Bootstrap()
login=LoginManager()
login.login_view = 'auth.login'
moment=Moment()
celery=Celery(__name__,broker=os.environ.get('CELERY_BROKER_URL','redis://localhost:6379/0'),backend=os.environ.get('CELERY_RESULT_BACKEND','redis://localhost:6379/0'))

def create_app(class_config=Config):
    app_instance=Flask(__name__)
    app_instance.config.from_object(class_config)
    db.init_app(app_instance)
    migrate.init_app(app_instance,db)
    bootstrap.init_app(app_instance)
    login.init_app(app_instance)
    moment.init_app(app_instance)

    app_instance.elasticsearch=Elasticsearch([app_instance.config['ELASTICSEARCH_URL']]) \
        if app_instance.config['ELASTICSEARCH_URL'] else None
    # print(app_instance.elasticsearch)


    celery.conf.update(app_instance.config)    

    from app.auth import bp as auth_bp
    app_instance.register_blueprint(auth_bp)

    from app.errors import bp as errors_bp
    app_instance.register_blueprint(errors_bp)

    from app.main import bp as main_bp
    app_instance.register_blueprint(main_bp)

    from app.background import bp as back_bp
    app_instance.register_blueprint(back_bp)

    if not app_instance.debug:
       if not os.path.exists('logs'):
           os.mkdir('logs')
       file_handler = RotatingFileHandler('logs/hackernews.log', maxBytes=10240,
                                       backupCount=10)
       file_handler.setFormatter(logging.Formatter(
           '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
       file_handler.setLevel(logging.DEBUG)
       log = logging.getLogger('werkzeug')
       log.setLevel(logging.DEBUG)
       log.addHandler(file_handler)
       log.info('new logger by werkzeug')

       app_instance.logger.addHandler(file_handler)
       app_instance.logger.setLevel(logging.DEBUG)
       app_instance.logger.info('hackernews site')

    return app_instance

from app import models