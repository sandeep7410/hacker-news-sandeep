from app import db
from app.main import bp
from flask import render_template, request, url_for, redirect, flash, current_app, abort
from flask_login import login_required, current_user
from app.models import Post, Comment, User
from app.main.forms import SubmitForm, CommentForm, SearchForm, EditProfileForm
from datetime import datetime
from werkzeug.urls import url_parse
import json
from flask import g
from flask import send_file, make_response
from flask import jsonify
from collections import OrderedDict
from sqlalchemy import and_


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for(
        'main.index', page=posts.next_num) if posts.has_next else None
    prev_url = url_for(
        'main.index', page=posts.prev_num) if posts.has_prev else None

    return render_template('index.html', posts=posts.items, url_parse=url_parse, next_url=next_url, prev_url=prev_url)


@bp.route('/submit', methods=['GET', 'POST'])
@login_required
def submit(title='', url='', text=''):
    form = SubmitForm()
    if form.validate_on_submit():
        if form.url.data and form.text.data or (not form.url.data and not form.text.data):
            flash('Enter either an url or text')
            return redirect(url_for('main.submit', title=form.title.data, url=form.url.data, text=form.text.data))
        post = Post(title=form.title.data, url=form.url.data,
                    text=form.text.data, post_author=current_user)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('main.postpage', post_id=post.id))
    else:

        form.title.data = request.args['title']
        form.url.data = request.args['url']
        form.text.data = request.args['text']

        return render_template('submit.html', title='Submit', form=form)


@bp.route('/user/<username>', methods=['GET'])
@login_required
def user(username):
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username).first()
    posts = user.post.order_by(Post.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for(
        'main.user', page=posts.next_num) if posts.has_next else None
    prev_url = url_for(
        'main.user', page=posts.prev_num) if posts.has_prev else None
    return render_template('user.html', posts=posts.items, user=user, url_parse=url_parse, next_url=next_url, prev_url=prev_url)


@bp.route('/comments')
def commentpage():
    page = request.args.get('page', 1, type=int)

    comments = Comment.query.order_by(Comment.id.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for(
        'main.commentpage', page=comments.next_num) if comments.has_next else None
    prev_url = url_for(
        'main.commentpage', page=comments.prev_num) if comments.has_prev else None
    return render_template('comments.html', comments=comments.items, title='Comments', url_parse=url_parse, next_url=next_url, prev_url=prev_url)


@bp.route('/site/<site_name>')
def site(site_name):
    page = request.args.get('page', 1, type=int)
    posts = Post.query.filter(Post.url.like('%{}%'.format(site_name))).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for(
        'main.user', page=posts.next_num) if posts.has_next else None
    prev_url = url_for(
        'main.user', page=posts.prev_num) if posts.has_prev else None
    return render_template('index.html', posts=posts.items, url_parse=url_parse, next_url=next_url, prev_url=prev_url)


@bp.route('/<post_id>/<comment_id>/reply', methods=['GET', 'POST'])
@login_required
def reply(comment_id, post_id):
    form = CommentForm()
    comment = Comment.query.get_or_404(comment_id)

    if form.validate_on_submit():
        reply = Comment(post_id=comment.post_id, parent_comment_id=comment_id,
                        comment=form.comment.data, user_id=current_user.id)
        db.session.add(reply)
        db.session.commit()
        return redirect(url_for('main.postpage', post_id=comment.post_id))
    return render_template('reply.html', title='Reply', form=form, comment=comment, url_parse=url_parse)


@bp.route('/post/<post_id>', methods=['GET', 'POST'])
def postpage(post_id):
    post = Post.query.get_or_404(post_id)
    mc = comments_to_nested_dict_by_post_id(post_id)
    form = CommentForm()
    if form.validate_on_submit():
        comment = Comment(user_id=current_user.id,
                          post_id=post_id, comment=form.comment.data)
        db.session.add(comment)
        db.session.commit()
        return redirect(url_for('main.postpage', post_id=post_id))
    return render_template('post.html', form=form, post=post, url_parse=url_parse, nested_dict=mc)


def comments_to_nested_dict_by_post_id(post_id):
    top_comments = get_top_level_comments_for_post_id(post_id)
    low_comments = get_lo_level_comments(post_id)
    var = low_level_comments_to_nested_dict(low_comments)
    all_comments = nested_top_comments(top_comments, var)
    return all_comments


def get_top_level_comments_for_post_id(post_id):
    comments = Comment.query.filter(
        and_(Comment.post_id == post_id, Comment.parent_comment_id == None)).all()
    comment_dict = {}
    for comment in comments:
        value = comment._asdict()
        comment_dict[comment.id] = value
    return comment_dict


def get_lo_level_comments(post_id):
    comments = Comment.query.filter(and_(
        Comment.post_id == post_id, Comment.parent_comment_id != None)).order_by(Comment.id.asc()).all()
    comment_dict = {}
    for comment in comments:
        value = comment._asdict()
        comment_dict[comment.id] = value
    return comment_dict


def low_level_comments_to_nested_dict(comments):
    for comment_id, v in reversed(OrderedDict(comments).items()):
        pid = v['parent_comment_id']
        if pid in comments:
            comments[pid]['reply'][comment_id] = v
            comments[pid]['reply'].move_to_end(comment_id, last=False)
            del comments[comment_id]
    return comments


def nested_top_comments(top_comments, comments):
    for k in comments:
        pid = comments[k]['parent_comment_id']
        top_comments[pid]['reply'][k] = comments[k]
    return top_comments


@bp.before_app_request
def before_request():
    g.search_form = SearchForm()
    if current_user.is_authenticated:
        db.session.commit()


@bp.route('/search')
def search():
    if not g.search_form.validate():
        return redirect(url_for('/main.search'))
    page = request.args.get('page', 1, type=int)
    posts, total = Post.search(
        g.search_form.q.data, page, current_app.config['POSTS_PER_PAGE'])

    next_url = url_for('main.search', q=g.search_form.q.data, page=page+1)\
        if total > page * current_app.config['POSTS_PER_PAGE'] else None

    prev_url = url_for('main.search', q=g.search_form.q.data, page=page-1)\
        if page > 1 else None
    return render_template('search.html', title='Search', posts=posts, next_url=next_url, prev_url=prev_url, url_parse=url_parse)


@bp.route('/user/<username>/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile(username):
    if username != current_user.username:
        flash('you are NOT {}!'.format(post.post_author.username))
        abort(403)
    form = EditProfileForm(username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        return redirect(url_for('main.user', username=current_user.username))
    elif request.method == 'GET':
        form.username.data = current_user.username
    return render_template('edit_profile.html', title='Edit Profile', form=form)


@bp.route('/post/<post_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_post(post_id):
    form = SubmitForm()
    post = Post.query.get(post_id)
    if current_user.id != post.post_author.id:
        flash('you are NOT {}!'.format(post.post_author.username))
        abort(403)
    if form.validate_on_submit():
        if form.url.data and form.text.data or (not form.url.data and not form.text.data):
            flash('Enter either an url or text')
            return redirect(url_for('main.submit', title=form.title.data, url=form.url.data, text=form.text.data))
        else:
            post.title = form.title.data
            post.url = form.url.data
            post.text = form.text.data
            db.session.commit()
            return redirect(url_for('main.postpage', post_id=post.id))
    else:
        form.title.data = post.title
        form.url.data = post.url
        form.text.data = post.text
        return render_template('submit.html', title='Edit Post', form=form)


@bp.route('/comment/<comment_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_comment(comment_id):
    form = CommentForm()
    comment = Comment.query.get(comment_id)
    if current_user.id != comment.comment_author.id:
        flash('you are NOT {}!'.format(comment.comment_author.username))
        abort(403)
    if form.validate_on_submit():
        comment.comment = form.comment.data
        comment.timestamp = datetime.utcnow()
        db.session.commit()
        return redirect(url_for('main.postpage', post_id=comment.post_id))
    else:
        form.comment.data = comment.comment
        return render_template('reply.html', title='Edit Comment', form=form, comment=comment, url_parse=url_parse)
