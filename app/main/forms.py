from flask_wtf import FlaskForm
from app.models import Post,User
from wtforms import TextAreaField,DateTimeField,StringField,SubmitField
from wtforms.validators import DataRequired, Length,URL,Optional,ValidationError
from flask import request

class EditProfileForm(FlaskForm):
    username=StringField('Username',validators=[DataRequired()])
    about_me=TextAreaField('About me',validators=[Length(min=0,max=160)])
    submit=SubmitField('Submit')

    def __init__(self,original_username,*args,**kwargs):
        super(EditProfileForm,self).__init__(*args,**kwargs)
        self.original_username=original_username
    
    def validate_username(self,username):
        if username.data!=self.original_username:
            user=User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError('Please use a different username.')

class SubmitForm(FlaskForm):
    title=StringField('Title',validators=[DataRequired()])
    url=StringField('URL',validators=[URL(require_tld=False,message='Enter an valid url'),Optional(strip_whitespace=True)])
    text=StringField('Text',validators=[Length(min=0,max=500)])
    submit=SubmitField('Submit')

class CommentForm(FlaskForm):
    comment=StringField('Your Comment',validators=[Length(min=0,max=500)])
    submit=SubmitField('Submit')

class SearchForm(FlaskForm):
    q=StringField('Search',validators=[DataRequired()])

    def __init__(self,*args,**kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata']=request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled']=False
        super(SearchForm,self).__init__(*args,**kwargs)
