from flask import current_app
from flask_login import UserMixin
from hashlib import md5
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from app import db
from app import login
from flask_login import login_manager
from app.search import add_to_index, remove_from_index, query_index
from collections import OrderedDict

class SearchableMixin(object):
    @classmethod
    def search(cls, expression, page, per_page):
        ids, total = query_index(cls.__tablename__, expression, page, per_page)
        if total == 0:
            return cls.query.filter_by(id=0), 0

        when = []
        for i in range(len(ids)):
            when.append((ids[i], i))
        print(when)
        query=cls.query.filter(cls.id.in_(ids)).order_by(db.case(when, value=cls.id))
        return query, total

    @classmethod
    def before_commit(cls, session):
        session._changes = {
            'add': list(session.new),
            'update': list(session.dirty),
            'delete': list(session.deleted)
        }

    @classmethod
    def after_commit(cls, session):
        for obj in session._changes['add']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['update']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['delete']:
            if isinstance(obj, SearchableMixin):
                remove_from_index(obj.__tablename__, obj)
        session._changes = None

    @classmethod
    def reindex(cls):
        for obj in cls.query:
            add_to_index(cls.__tablename__, obj)


db.event.listen(db.session, 'before_commit', SearchableMixin.before_commit)
db.event.listen(db.session, 'after_commit', SearchableMixin.after_commit)


class User(SearchableMixin, UserMixin, db.Model):
    __searchable__ = ['username']
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True, index=True)
    password_hash = db.Column(db.String())
    post = db.relationship('Post', backref='post_author', lazy='dynamic')
    comment = db.relationship(
        'Comment', backref='comment_author', lazy='dynamic')

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Post(SearchableMixin,db.Model):
    __searchable__ = ['title', 'url', 'text']
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128))
    url = db.Column(db.String())
    text = db.Column(db.String())
    timestamp = db.Column(db.DateTime, default=datetime.utcnow())
    post_for_a_comment = db.relationship(
        'Comment', backref='post_for_a_comment', lazy='dynamic')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    comment_post = db.relationship(
        'Comment', backref='comment_post', lazy='dynamic')

    def __repr__(self):
        return '<Post {}>'.format(self.title)

    def _asdict(self):
        return {
            'id': self.id,
            'title': self.title,
            'url': self.url,
            'text': self.text,
            'timestamp': self.timestamp.strftime("%d-%m-%Y %H:%M:%S"),
            'username': self.post_author.username
        }


class Comment(SearchableMixin,db.Model):
    __searchable__ = ['comment']
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    parent_comment_id = db.Column(db.Integer, db.ForeignKey('comment.id'))
    timestamp = db.Column(db.DateTime, default=datetime.utcnow())
    comment = db.Column(db.String())

    def _asdict(self):
        return {
            'username': self.comment_author.username,
            'post_id': self.post_id,
            'comment': self.comment,
            'timestamp': self.timestamp,
            'parent_comment_id': self.parent_comment_id,
            'reply': OrderedDict()}


@login.user_loader
def load_user(id):
    return User.query.get(int(id))
