from app.errors import bp
from flask import render_template
from app import db

@bp.app_errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'),404

@bp.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('errors/500.html'),500

@bp.app_errorhandler(403)
def not_authorised(error):
    return render_template('errors/403.html'),403

@bp.app_errorhandler(405)
def not_allowed(error):
    return render_template('errors/405.html'),405