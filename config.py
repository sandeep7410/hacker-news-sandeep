import os
from dotenv import load_dotenv
basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

class Config:
    POSTS_PER_PAGE=30
    SECRET_KEY=os.environ.get('SECRET_KEY')
    
    SQLALCHEMY_DATABASE_URI=os.environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    
    ELASTICSEARCH_URL=os.environ.get('ELASTICSEARCH_URL')
    
    CELERY_BROKER_URL=os.environ.get('CELERY_BROKER_URL')
    CELERY_RESULT_BACKEND=os.environ.get('CELERY_RESULT_BACKEND')
    
    MAIL_SERVER=os.environ.get('MAIL_SERVER')
    MAIL_PORT=int(os.environ.get('MAIL_PORT')or 25)
    MAIL_USE_TLS=os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME=os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD=os.environ.get('MAIL_PASSWORD')
    ADMINS=['nallapati.sandeep@mountblue.io']