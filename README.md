# HACKER NEWS

## Description

This project allows the user to post his blog or other's so that all the other users can comment on that.

## Prerequisites

- HTTP requests,response
- get,post,redirect flow
- ajax
- celery

## Installation

1.  download the project into a folder
2.  create and activate virtual environment
3.  install all the dependencies using requirements.txt file
    `pip install -r requirements.txt`
4.  *optional* -> install elasticsearch in your local machine, else you can use its cloud services
5.  install redis server

## Configuration

1.  configure the database url in the terminal. *you can use any database*
2.  in a separate terminal configure the broker and backend url's


## Running

1.  in terminal goto the directory that contains `hackernews.py` module
2.  `flask run` will run the flask server in your machine
3.  in other terminal run the celery worker using the `celery -A app.background.tasks worker --loglevel=info`
4.  run the redis server 
5.  start the elasticsearch engine service `sudo systemctl start elasticsearch.service`

## Deployment
1.  clone this git repo into your local machine with https://gitlab.com/sandeep7410/hacker-news-sandeep.git
2.  enter the following commands to update and upgrade your system, so that it can support all the softwares this application is using
    `sudo apt-get update`
    `sudo apt-get upgrade`
3.  change the working directory to `hacker-news-sandeep` i.e., go inside the file you cloned now your pwd should like something this`/hacker-news-sandeep$`
4.  run `bash deploy.sh`, it asks for multiple downloads
5.  enter the public ip address of your system in the browser 

###Last step
create a `.env` file with the bellow foramt (without < >) in the present working directory
```
FLASK_APP=hackernews.py
DATABASE_URL='postgresql://postgres:<password>@127.0.0.1:5432/<database-name>'
SECRET_KEY='something sneeky'
CELERY_BROKER_URL='redis://localhost:6379/0'
CELERY_RESULT_BACKEND='redis://localhost:6379/0'

MAIL_SERVER='smtp.googlemail.com'
MAIL_PORT=587
MAIL_USE_TLS=True
MAIL_USERNAME='<your mail-id>'
MAIL_PASSWORD='<your mail password>' 
ADMINS=['<admins mail id>']
```

## Built with

- Visual studio
- google chrome devtools

**Authors:**

Sandeep -sandeep7410