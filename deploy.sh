#!/bin/bash

sudo apt-get update
sudo apt-get upgrade
#install python3
if ! which python3 > /dev/null; then
   echo -e "Command not found! Install? (y/n) \c"
   read
   if "$REPLY" = "y"; then
      sudo apt-get install python3
   else echo "can't continue without python3"
   fi
fi

#pip
sudo apt-get install python3-pip python3-setuptools
sudo apt-get install libpq-dev python-dev
sudo apt-get install python3-venv
export LC_CTYPE="en_US.UTF-8"
# sudo pip3 install virtualenv
#install postgres
sudo apt install postgresql postgresql-contrib
#db creation
sudo -u postgres psql -c "create database hackernews;"
#creating venv activating
python3 -m venv venv
source venv/bin/activate
mkdir data
pip install -r requirements.txt
pip install gunicorn
flask db init
flask db migrate
flask db upgrade

# install redis ans start server
#create redis.service
sudo apt-get install -y curl build-essential tcl
sudo apt-get install redis-server
sudo systemctl enable redis-server.service

# create hacker.service
sudo touch /etc/systemd/system/hackernews.service
sudo bash -c 'cat > /etc/systemd/system/hackernews.service <<EOF
[Unit]
Description=Gunicorn instance to serve hacker
After=network.target

[Service]
User=ubuntu
WorkingDirectory=$(pwd)
Environment="PATH=$(pwd)/venv/bin"
ExecStart=$(pwd)/venv/bin/gunicorn --workers 3 --bind localhost:5000 "wsgi:create_app()"

[Install]
WantedBy=multi-user.target
EOF'

#create celery worker
sudo touch /etc/systemd/system/celery-worker.service
sudo bash -c 'cat > /etc/systemd/system/celery-worker.service <<EOF
[Unit]
Description=celery instance to serve hackernews
After=network.target

[Service]
User=ubuntu
WorkingDirectory=$(pwd)
Environment="PATH=$(pwd)/venv/bin"
ExecStart=$(pwd)/venv/bin/celery -A app.background.tasks worker --loglevel=info

[Install]
WantedBy=multi-user.target
EOF'

sudo apt install nginx
# create nginx conf
sudo touch /etc/nginx/sites-available/hackernews.conf
sudo bash -c 'cat>/etc/nginx/sites-available/hackernews.conf <<EOF
server {
    listen 80;

    location / {
        include proxy_params;
        proxy_pass http://localhost:5000;
    }
}

EOF'
sudo systemctl daemon-reload

sudo systemctl restart redis-server.service

sudo rm /etc/nginx/sites-available/default
sudo rm /etc/nginx/sites-enabled/hackernews.conf /etc/nginx/sites-enabled/default
sudo ln -s /etc/nginx/sites-available/hackernews.conf /etc/nginx/sites-enabled/

sudo systemctl enable hackernews.service 
sudo systemctl restart hackernews.service

sudo systemctl enable celery-worker.service
sudo systemctl restart celery-worker.service

sudo systemctl enable nginx
sudo systemctl restart nginx
